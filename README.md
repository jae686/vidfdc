### What is this repository for? ###

* The Vollumetric Illusions Demo Framework for the Dreamcast is a data driven demo framework to make demos (as in demoscene demos)
* Current Version : 0.0.1

### How do I get set up? ###

* Requires the latest version of KOS
* Expat, built for the DC. A installer script is provided.
* Once the above requisites are satisfied, just run make.

### Current Features ###

* Scene and camera are described on a xml file, parsed on the DC on loading
* Automatic Asset loading from all supported files on the ramdisk
* OBJ mesh support
* Textured font support. You may generate them at [Littera](http://kvazars.com/littera/)


### Contribution guidelines ###

* Just don't break stuff.


### Who do I talk to? ###

* Repo owner