#include "data_estructures.h"
#include <string.h>

pmesh genGrid(char const *const name, float x_size, float y_size, int x_points, int y_points) {
	int total_elems = x_points * y_points;  // needed for malloc

	int seg_x = x_points - 1;
	int seg_y = y_points - 1;

	float seg_x_size = x_size / seg_x;
	float seg_y_size = y_size / seg_y;

	int curr_x = 0;
	int curr_y = 0;

	int curr_pos = 0;
	// allocate a pmesh

	pmesh ret = (pmesh)malloc(sizeof(mesh));
	vec3 *vertex_data = (vec3 *)malloc(sizeof(vec3) * total_elems);

	ret->uv_data = NULL;
	ret->vertex_color = NULL;
	ret->vertex_normal = NULL;
	ret->vertex_count = total_elems;
	strncpy(ret->name, name, sizeof(ret->name));
	ret->name[sizeof(ret->name) - 1] = 0;

	ret->indices_count = x_points;
	ret->uv_count = y_points;

	float x_coord = 0.0f;
	float y_coord = 0.0f;
	// iterate over all the vertex structure

	for(curr_x = 0; curr_x < x_points; ++curr_x) {
		x_coord = seg_x_size * curr_x;

		for(curr_y = 0; curr_y < y_points; ++curr_y) {
			y_coord = seg_y_size * curr_y;

			vertex_data[curr_pos].x = x_coord;
			vertex_data[curr_pos].y = y_coord;
			vertex_data[curr_pos].z = 0.0f;
			//	printf("x : %f, y : %f curr_pos : %d\n", vertex_data[curr_pos].x, vertex_data[curr_pos].y, curr_pos);
			curr_pos = curr_pos + 1;
		}
	}

	ret->vertex_data = (float *)vertex_data;

	return ret;
}
