#include <kos.h>
#include <GL/gl.h>

#ifndef VIDF_DATA_STRUCTURE
#define VIDF_DATA_STRUCTURE

#define TEXT_BUFFER_SIZE 64

// Render mode bytemasks (check for endianness issues)

#define DRAW_WIREFRAME 0x01
#define DRAW_FLAT 0x02
#define DRAW_GRID 0x04
#define DRAW_REGULAR 0x08
#define DRAW_2D_OVERLAY 0x10
#define SIMPLE_LIGHT 0x20

// flag bytemasks

#define NONE 0x00
#define MODEL 0x01
#define CAMERA 0x02

// flags para verif se e quais parametros foram ajustados

#define START_POS 0x01
#define END_POS 0x02
#define START_ROT 0x04
#define END_ROT 0x08
#define START_FOV 0x10
#define END_FOV 0x20
#define START_LOOK 0x40
#define END_LOOK 0x80

#define STATIC_POS 0x100
#define STATIC_ROT 0x200
#define STATIC_FOV 0x400

#define STATIC_ANGLE 0x800
#define PERTETUAL_ROTATION 0x1000

#define VEL_SET 0x2000

// mesh transform


#define ROTATION 0
#define TRANSLATION 1
#define SCALE 2

// light parameters

#define LIGHT_DIFFUSE 4
#define LIGHT_SPECULAR 8

// Data structures for the VIDF

// stuff imported from the previous toolkit attempt


typedef struct sStats {
	// Struct meant for VIDFDC performance profiling
	// TODO : Improve struct as needed

	unsigned int totalLoadingTime;
	unsigned int totalMeshesPerFrame;
	unsigned int totalVerticesPerFrame;
	unsigned int totalLightPerFrame;
	unsigned int averageRenderTime ;


} Stats, *pStats ;




typedef struct sLoadingStatus {
	unsigned int total_assets;
	unsigned int loaded_assets;

	float previous_percent;
	float current_percent;

} LoadingStatus;

// there is no need to make this with dynamic allocation.
// typedef LoadingStatus * pLoadingStatus;

typedef struct sIndicesTriplet {
	int v;
	int vn;
	int vt;
	int repetido;
} IndicesTriplet;

typedef IndicesTriplet *pIndicesTriplet;

typedef struct sVec2 {
	float u;
	float v;
} vec2;

typedef struct sVec3 {
	float x;
	float y;
	float z;
} vec3;

// transformation lists
typedef struct sTransformList {
	int transformType;
	float scalar_val;  // to use for rotation
	vec3 transformData;
	struct sTransformList *next;

} TransformList;

typedef TransformList *pTransformList;

typedef struct sMesh {
	float *vertex_data;
	float *uv_data;
	float *vertex_color;
	float *vertex_normal;
	unsigned int *vertex_indices;
	int indices_count;  // used on procedural.c
	int vertex_count;
	int uv_count;  // used on procedural.c
	char name[32];
	struct sMesh *next;

} mesh;

typedef mesh *pmesh;
// sTexture struct is not used anymore, since i've replaced my own png loader with SOIL
typedef struct sTexture {
	unsigned char *pixels;
	int height;
	int width;
	int color_type;
	int bit_depth;
	char name[32];
} texture;

typedef texture *ptexture;

// linked list for 3d Models

typedef struct s3DModelList {
	pmesh mesh;
	char *name;  // string for mesh identification
	struct s3DModelList *next;
} _3DModelList;

typedef _3DModelList *p3DModelList;

typedef struct sTextureList {
	GLuint texture_handle;  // OGL texture Handle
	// pvr_ptr_t texture_handle ; // KOS PVR Pointer : Apagar ?
	char *name;                 // string for texture identification
	struct sTextureList *next;  // the next texture of the list

	// hight and width
	int width;
	int height;

	//

} TextureList;

typedef TextureList *pTextureList;

pmesh retrieveMesh(p3DModelList l, int pos);
int retrieveTextureHandle(pTextureList l, int pos);

typedef struct sSceneDescription  // makes a association between a mesh, a texture, its location on the scene and a scale vector. <- this is used to feed the render loop.
    {

	char *name;
	char *mesh_name;
	char *texture_name;

	// depecrated : replaced with transform list
	/*
	vec3 pos;
	vec3 scale;
	vec3 rotation;
	vec3 direction;
	*/
	vec3 speed;
	vec3 rot_speed;

	float angle;

	int render_effects;  // BYTE MASK with effect setting (wireframe, flash shaded, etc)
	int flags;           // BYTE MASK
	float v;             // velocity obsolete, replaced for a vec3
	char t;              // transform (if v should be applied to pos, rot, or scale - to be replaced for a long int.
	pTransformList transforms;
	struct sSceneDescription *next;

	// in order to support 2D overlays, the variables below had to be added
	// these values can also be used for 3d meshes 
	//TODO :: Add suport for these on the dc_render.c render loop and parser.
	uint64 start;
	uint64 end;
	uint64 fade_in;
	uint64 fade_out;
	//

} SceneDescription;

typedef SceneDescription *pSceneDescription;

int getMeshPos(p3DModelList l, char *name);
int getTexturePos(pTextureList l, char *name);

pSceneDescription newSceneDescription();
pSceneDescription addToSceneDescriptionList(pSceneDescription head, pSceneDescription item);
void addToSceneDescriptionName(pSceneDescription sd, char const *const name);
void addToSceneDescriptionMesh(pSceneDescription sd, char const *const mesh_name);
void addToSceneDescriptionTextureName(pSceneDescription sd, char const *const texture_name);
void addToSceneDescriptionPos(pSceneDescription sd, vec3 pos);
void addToSceneDescriptionScale(pSceneDescription sd, vec3 scale);
void addToSceneDescriptionRot(pSceneDescription sd, vec3 rotation);
void addToSceneDescriptionDir(pSceneDescription sd, vec3 direction);
void addToSceneDescriptionSpeed(pSceneDescription sd, vec3 speed);
void setSceneDescriptionRender(pSceneDescription a, int mode);
void setSceneDescriptionAngle(pSceneDescription a, float angle);

// Camera lists

typedef struct sCameraList {
	vec3 start_pos;
	vec3 end_pos;

	vec3 start_looking_direction;
	vec3 end_looking_direction;

	vec3 start_rotation;
	vec3 end_rotation;

	vec3 velocity;  // with the start and end positions plus the time, speed value comes for free.
	vec3 velocity_lookat ; //needed to update the lookat vector!
	vec3 accell;    // not for implementation yet

	unsigned long time_start;
	unsigned long time_end;

	float start_fov;
	float end_fov;

	float angle;

	int id;  // stopping to use strings to identify stuff.

	int flags;

	struct sCameraList *next;

} CameraList, *pCameraList;

typedef struct sTexCharCoord {
	float x;
	float y;
	float width;
	float height;
} TexCharCoord;

typedef TexCharCoord *pTexCharCoord;

typedef struct sTexFontList {
	char *name;
	int texture_handle;
	TexCharCoord charCoord[255];

	struct sTexFontList *next;

} TexFontList, *pTexFontList;

// typedef TexFont * pTexFont;

typedef struct sDemoAssetList  // the structure thats holds the linked lists (of all demo assets)
    {
	p3DModelList m;
	pTextureList t;
	pSceneDescription d;
	pCameraList c;
	unsigned long milisec_elapsed;
	pTexFontList f;
	LoadingStatus loading_status;
	pStats s;

} DemoAssetList;

typedef DemoAssetList *pDemoAssetList;

void setCameraFlag(pCameraList c, const int f);
void SetSceneDescriptorFlag(pSceneDescription sd, const int f);

vec3 getSpeedVec3(vec3 start_pos, vec3 end_pos, unsigned long start_time, unsigned long end_time);

void CameraListPosUpdate(pCameraList cl, unsigned long curr_time);

pTransformList addTransform(pTransformList l, int transformType, vec3 v, float scalar);

pTexFontList newTexfont(char *name);

inline int retrieveTextureWidth(pTextureList a, int b);

inline int retrieveTextureHeight(pTextureList a, int b);

p3DModelList add_pmesh(p3DModelList l, pmesh m, char const *const name);

char *retrieveTextureName(pTextureList l, int pos);

int isCameraInTime(pCameraList l, const unsigned long curr_time);

pCameraList newCamera();
void setCameraStartTime(pCameraList l, long int a);
void setCameraEndTime(pCameraList l, long int a);
void setCameraPosStart(pCameraList l, vec3 start_pos);
void setCameraPosEnd(pCameraList l, vec3 end_pos);
void setCameraLookingDirectionStart(pCameraList l, vec3 start);
void setCameraLookingDirectionEnd(pCameraList l, vec3 end);
pCameraList addToCameraList(pCameraList head, pCameraList item);

void addToSceneDescriptor2DoverlayStart(pSceneDescription sd, uint64 a);
void addToSceneDescriptor2DoverlayEnd(pSceneDescription sd, uint64 a);
void addToSceneDescriptor2DoverlayFade_In(pSceneDescription sd, uint64 a);
void addToSceneDescriptor2DoverlayFade_Out(pSceneDescription sd, uint64 a);

void addToSceneDescriptorStartTime(pSceneDescription sd, uint64 a); 
void addToSceneDescriptorEndTime(pSceneDescription sd, uint64 a);
void addToSceneDescriptorFade_In(pSceneDescription sd, uint64 a);
void addToSceneDescriptorFade_Out(pSceneDescription sd, uint64 a);

pStats initStats();
void resetStatsPerFrame(pStats s);
void addMeshStat(pStats s, pmesh p);


#endif
