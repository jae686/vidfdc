#ifndef PCXLOADER_H
#define PCXLOADER_H

#include <GL/gl.h>

int loadtxr(const char *fn, GLuint *txr, unsigned int *w, unsigned int *h);

#endif
