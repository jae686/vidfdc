#ifndef PNGLOADER_H
#define PNGLOADER_H

#include <GL/gl.h>

int loadpng(const char *fn, GLuint *txr, int *w, int *h);

#endif
