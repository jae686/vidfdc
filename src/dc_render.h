#ifndef DCRENDER_H
#define DCRENDER_H

#include "data_estructures.h"

void init_screen();
int demo_render(pDemoAssetList a);
void render_function();
inline unsigned long diff_msec(uint64 start, uint64 end);
void drawQuad(float x1, float y1, float x2, float y2);
void drawSprite(float x1, float y1, float x2, float y2, float u1, float v1, float u2, float v2);

#endif
