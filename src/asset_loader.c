// asset loading functions
#include <kos.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

#include "data_estructures.h"

#include "xml_parser.h"
#include "fnt_parser.h"
#include "obj_loader.h"
#include "pcx_loader.h"
#include "png_loader.h"
#include "loading_screen.h"

extern pDemoAssetList demo_content;

const char *asset_dir = "rd/";

void TexFontInspector(pTexFontList l)  // FOR DELETION
{
	pTexFontList curr = l;
	int n = 0;
	while(curr != NULL) {
		printf("Name : %s\n", curr->name);
		printf("tex_handle : %d\n", curr->texture_handle);
		for(n = 0; n < 255; n++) {
			printf("\t x : %f y : %f w : %f h : %f \n", (double)curr->charCoord[n].x, (double)curr->charCoord[n].y, (double)curr->charCoord[n].width, (double)curr->charCoord[n].height);
		}
		n = 0;
		curr = curr->next;
	}
}

pTexFontList add_texture_font(pTexFontList l, char *filename, char *id_name) {

	// allocate and load the texture file before list insertion
	pTexFontList newFontListItem = newTexfont(id_name);

	load_font(filename, newFontListItem, id_name);  // modifies newFontListItem contents

	// TexFontInspector(newFontListItem);

	if(l == NULL) {
		return newFontListItem;
	} else {
		pTexFontList curr = l;
		while(curr->next != NULL) {
			curr = (pTexFontList)curr->next;
		}
		curr->next = newFontListItem;
		return l;
	}
}

pTextureList add_texture_pcx(pTextureList l, char *filename, char *id_name) {

	pTextureList newTextureItem;  // finish this part!
	newTextureItem = (pTextureList)malloc(sizeof(TextureList));
	newTextureItem->next = NULL;

	newTextureItem->name = strdup(id_name);

	GLuint t;
	unsigned int h, w;
	loadtxr(filename, &t, &w, &h);
	newTextureItem->width = w;
	newTextureItem->height = h;
	newTextureItem->texture_handle = t;

	if(l == NULL) {
		return newTextureItem;
	} else {
		pTextureList curr = l;
		while(curr->next != NULL) {
			curr = curr->next;
		}
		curr->next = newTextureItem;
		return l;
	}
}

pTextureList add_texture_png(pTextureList l, char *filename, char *id_name) {

	pTextureList newTextureItem;  // finish this part!
	newTextureItem = (pTextureList)malloc(sizeof(TextureList));
	newTextureItem->next = NULL;

	newTextureItem->name = strdup(id_name);

	GLuint t;
	int h, w;

	loadpng(filename, &t, &w, &h);

	newTextureItem->width = w;
	newTextureItem->height = h;
	newTextureItem->texture_handle = t;

	if(l == NULL) {
		return newTextureItem;
	} else {
		pTextureList curr = l;
		while(curr->next != NULL) {
			curr = curr->next;
		}
		curr->next = newTextureItem;
		return l;
	}
}

p3DModelList add_model_obj(p3DModelList l, char *filename, char *id_name)  // loads a 3dmodel vie3d file from the hd puts it on the linked list. the function returns the list's head
                                                                       // the name arg is a name for 3dmesh identification with the framework
{

	p3DModelList newListElem = (p3DModelList)malloc(sizeof(_3DModelList));  // allocate the new list ellement.....

	// newListElem->mesh = load_from_custom_3d(filename); 		// the model is loaded into RAM......
	newListElem->mesh = obj_loader(filename);

	newListElem->name = (char *)malloc(strlen(id_name));  // make a string large enough to hold name
	strcpy(newListElem->name, id_name);                   // done the list element has the 3dmesh and the name.
	newListElem->next = NULL;
	// lets take a look at that list.....
	int pos = 0;  // for debug
	// empty list?
	if(l == NULL) {
		printf("Model loaded at pos : %d, name : %s \n - NULL POINTER \n", pos, newListElem->name);
		return newListElem;
	} else {
		p3DModelList curr_elem;

		pos++;  // here we are past pos = 0.

		for(curr_elem = l; curr_elem->next != NULL; curr_elem = curr_elem->next)  // get to the last element of the list
		{
			pos++;
		}

		// now that we are at the last element, lets add our recently loaded mesh
		curr_elem->next = newListElem;
		printf("Model loaded at pos : %d, name : %s \n", pos, newListElem->name);
		return l;  // returns the list head.
	}
}

pDemoAssetList init_AssetList()  // initializes the asset list
{
	pDemoAssetList demo_asset = (pDemoAssetList)malloc(sizeof(DemoAssetList));  // allocating the pointer that's returned

	// initialize the data structures
	demo_asset->m = NULL;
	demo_asset->t = NULL;
	demo_asset->d = NULL;
	demo_asset->c = NULL;
	demo_asset->milisec_elapsed = 0;
	demo_asset->f = NULL;

	demo_asset->loading_status.total_assets = 0;
	demo_asset->loading_status.loaded_assets = 0;
	demo_asset->loading_status.previous_percent = 0.0;
	demo_asset->loading_status.current_percent = 0.0;
	demo_asset->s = initStats();
	return demo_asset;
}

void setTotalAssets(pDemoAssetList d, unsigned int a) { d->loading_status.total_assets = a; }

void setLoadedAssets(pDemoAssetList d, unsigned int a) { d->loading_status.loaded_assets = a; }

void setPreviousPercent(pDemoAssetList d, float a) { d->loading_status.previous_percent = a; }

void setCurrentPercent(pDemoAssetList d, float a) { d->loading_status.current_percent = a; }

void incrementLoadAsset(pDemoAssetList d) {
	d->loading_status.loaded_assets++;
	float percent = (float)d->loading_status.loaded_assets / (float)d->loading_status.total_assets;

	setPreviousPercent(d, d->loading_status.current_percent);
	setCurrentPercent(d, percent);
}

pDemoAssetList asset_loader(pDemoAssetList demo_asset)  // loads all the files onto the assets/ dir, using the aprorpiate loaders, selected by their extention. returns the struct with pointers to the assets
{

	DIR *dir;

	unsigned int number_of_files = 0;
	char *nome_ficheiro;
	char *extencao;
	struct dirent *ent;
	dir = opendir(asset_dir);
	int debug_tex = 0;

	if(dir != NULL) {

		/* print all the files and directories within directory */
		while((ent = readdir(dir)) != NULL) {
			if(strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0)
				number_of_files++;
		}
		printf("Descobertos %d ficheiros para carregar\n", number_of_files);
		setTotalAssets(demo_asset, number_of_files);
		rewinddir(dir);
	}

	if(dir != NULL) {

		/* print all the files and directories within directory */
		while((ent = readdir(dir)) != NULL) {

			int fullpath_size = 0;

			char *filename = strdup(ent->d_name);  // ensure its NULL terminated
			fullpath_size = strlen(filename) + strlen(asset_dir);
			char *fullpath = (char *)malloc(fullpath_size + 1);  // creates an array with enough space for the filename
			fullpath[fullpath_size] = '\0';                      // NULL TERMINATOR

			if(strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name, "..") != 0) {
				sprintf(fullpath, "%s%s", asset_dir, ent->d_name);  // now we have our "fullpath" filename to pass to the respective loading fuction.
				nome_ficheiro = strtok(ent->d_name, ".");
				extencao = strtok(NULL, ".");

				// now that we have both the full path, and the file extention, we can start to send the proper path to the correct loader
				if(strcmp(extencao, "obj") == 0) {
					demo_asset->m = add_model_obj(demo_asset->m, fullpath, nome_ficheiro);
				} else if(strcmp(extencao, "pcx") == 0) {
					debug_tex++;
					demo_asset->t = add_texture_pcx(demo_asset->t, fullpath, nome_ficheiro);

				} else if(strcmp(extencao, "xml") == 0) {
					printf("loading xml\n");
					init_parser(fullpath, demo_asset);
				} else if(strcmp(extencao, "fnt") == 0) {
					printf("loading fnt\n");
					demo_asset->f = add_texture_font(demo_asset->f, fullpath, nome_ficheiro);
				} else if(strcmp(extencao, "png") == 0) {
					demo_asset->t = add_texture_png(demo_asset->t, fullpath, nome_ficheiro);
				}

				incrementLoadAsset(demo_asset);
				drawLoadingBar(demo_asset);
			}
		}
		closedir(dir);

		return demo_asset;
	} else { /* could not open directory */
		perror("");
		return NULL;
	}
}
