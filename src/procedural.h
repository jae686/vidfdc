#ifndef PMESH_H
#define PMESH_H

#include "data_estructures.h"

pmesh genGrid(char const *const name, float x_size, float y_size, int x_points, int y_points);

#endif
