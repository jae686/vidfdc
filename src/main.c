
#include <kos.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <dc/pvr.h>

#include "asset_loader.h"

#include "dc_render.h"

#include "data_estructures.h"

#include "matrix.h"

extern uint8 romdisk[];

KOS_INIT_FLAGS(INIT_DEFAULT | INIT_MALLOCSTATS);
KOS_INIT_ROMDISK(romdisk);

pDemoAssetList demo_content;

pvr_init_params_t params = {
	/* Enable opaque and translucent polygons with size 16 */
	{ PVR_BINSIZE_16, PVR_BINSIZE_0, PVR_BINSIZE_16, PVR_BINSIZE_0, PVR_BINSIZE_0 },

	/* Vertex buffer size 512K */
	512 * 1024,

	/* DMA enabled? */
	0,

	/* Horizontal scaling */
	0
};

int main(int argc, char *argv[]) {
	glKosInit();

	// Load assets
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, 640, 0, 480, -1, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	demo_content = init_AssetList();
	asset_loader(demo_content);

	// Run demo
	int ret = demo_render(demo_content);
	if(ret) {
		puts("Error while running demo.");
	}

	return ret;
}
