#include <kos.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <png/png.h>
#include <dc/pvr.h>

int loadpng(const char *fn, GLuint *txr, unsigned *w, unsigned *h) {
	kos_img_t imgaddr;

	if(png_to_img(fn, PNG_FULL_ALPHA, &imgaddr) < 0) {
		printf("can't load %s\n", fn);
		return 1;
	}

	printf("Loading %lux%lu PNG `%s'..\n", imgaddr.w, imgaddr.h, fn);

	*w = imgaddr.w;
	*h = imgaddr.h;

	glGenTextures(1, txr);
	glBindTexture(GL_TEXTURE_2D, *txr);

	if(imgaddr.fmt == KOS_IMG_FMT_ARGB4444) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imgaddr.w, imgaddr.h, 0, GL_RGBA, GL_UNSIGNED_SHORT_4_4_4_4, imgaddr.data);
	} else {
		puts("Unhandled texture format.");
		return 1;
	}

	return 0;
}
