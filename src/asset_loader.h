#include "data_estructures.h"

pDemoAssetList init_AssetList();
pDemoAssetList asset_loader(pDemoAssetList demo_asset);

void setTotalAssets(pDemoAssetList d, unsigned int a);
void setLoadedAssets(pDemoAssetList d, unsigned int a);
void setPreviousPercent(pDemoAssetList d, float a);
void setCurrentPercent(pDemoAssetList d, float a);