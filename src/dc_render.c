#include <kos.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <sys/time.h>
#include <time.h>
#include <sys/time.h>
//#include <arch/time.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <pcx/pcx.h>
#include <dc/pvr.h>

#include <math.h>

#include "data_estructures.h"
#include "matrix.h"
#include "cube.h"


// GL Lightening Test
/*
GLfloat light_pos[] = {1.0 , 1.0, 1.0, 0.0};
GLfloat light_color[] = {1.0 , 1.0, 1.0, 1.0};
*/

// cube.h helper define
#define BUFFER_OFFSET(x)((char *)NULL+(x))


typedef struct {
	float x, y;
	float u, v;
} Vertex2C2T;

void pmesh_draw_array();
void pmesh_draw(pmesh p);
void pmesh_draw_test();
void draw_p(pmesh m);
extern pDemoAssetList demo_content;

float projectionMatrix[16];
static Vertex2C2T spriteVerts[4];

static unsigned short SCREEN_W = 640;
static unsigned short SCREEN_H = 480;

static void setVertex2C2T(Vertex2C2T *vert, float x, float y, float u, float v) {
	vert->x = x;
	vert->y = y;
	vert->u = u;
	vert->v = v;
}

void drawQuad(float x1, float y1, float x2, float y2) {
	setVertex2C2T(&spriteVerts[0], x1, y2, 0, 0);
	setVertex2C2T(&spriteVerts[1], x1, y1, 0, 0);
	setVertex2C2T(&spriteVerts[2], x2, y2, 0, 0);
	setVertex2C2T(&spriteVerts[3], x2, y1, 0, 0);

// Detect kgl which has a buggy glDrawArrays implementation right now, fall back to immediate mode
#ifndef GLAPIENTRY
	unsigned i;
	glBegin(GL_TRIANGLE_STRIP);
	for(i = 0; i < 4; ++i) {
		glVertex2fv((float *)&spriteVerts[i].x);
	}
	glEnd();

// Use glDrawArrays with other implementations
#else
	glVertexPointer(2, GL_FLOAT, sizeof(Vertex2C2T), spriteVerts);
	glEnableClientState(GL_VERTEX_ARRAY);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glDisableClientState(GL_VERTEX_ARRAY);
#endif
}

void drawSprite(float x1, float y1, float x2, float y2, float u1, float v1, float u2, float v2) {
	setVertex2C2T(&spriteVerts[0], x1, y2, u1, v2);
	setVertex2C2T(&spriteVerts[1], x1, y1, u1, v1);
	setVertex2C2T(&spriteVerts[2], x2, y2, u2, v2);
	setVertex2C2T(&spriteVerts[3], x2, y1, u2, v1);

	glEnable(GL_TEXTURE_2D);

// Detect kgl which has a buggy glDrawArrays implementation right now, fall back to immediate mode
#ifndef GLAPIENTRY
	unsigned i;
	glBegin(GL_TRIANGLE_STRIP);
	for(i = 0; i < 4; ++i) {
		glTexCoord2f(spriteVerts[i].u, spriteVerts[i].v);
		glKosVertex2f(spriteVerts[i].x, SCREEN_H - spriteVerts[i].y);
	}
	glEnd();

// Use glDrawArrays with other implementations for better performance
#else
	glVertexPointer(2, GL_FLOAT, sizeof(Vertex2C2T), spriteVerts);
	glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex2C2T), (char *)spriteVerts + 2 * sizeof(float));

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glDisableClientState(GL_VERTEX_ARRAY);

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
#endif

	glDisable(GL_TEXTURE_2D);
}

extern GLuint glTextureLoadPVR(char *fname, unsigned char isMipMapped, unsigned char glMipMap);

int is_it_on_time(uint64 curr_time, int64 start, int64 end) { return curr_time >= start && curr_time <= end; }

float opacity_level(uint64 curr_time, uint64 start, uint64 end, uint64 fade_in, uint64 fade_out) {
	uint64 abs_fade_in = start + fade_in;
	uint64 abs_fade_out = end - fade_out;

	// Before start or after end -> 0 opacity
	if(curr_time <= start || curr_time >= end)
		return 0.f;

	// Fade in
	else if(curr_time <= abs_fade_in)
		return (float)(curr_time - start) / fade_in;

	// Peak
	else if(curr_time <= abs_fade_out)
		return 1.f;

	// Fade out
	else
		return 1.f - (float)(curr_time - abs_fade_out) / fade_out;
}

void texture_overlay(int curr_texture, float x1_coord, float y1_coord, float x2_coord, float y2_coord, float opacity) {
	// places a texture on the screen, in ortographic mode
	//  A(x1, y1)-------------------------------------C(x2, y1)
	//      |                                            |
	//      |     <quad with the texture mapped          |
	//      |                                            |
	//      |                                            |
	//  B(x1, y2)-------------------------------------D(x2, y2)

	// int curr_texture_pos = getTexturePos(t, texture_name);
	// int curr_texture = retrieveTextureHandle(t, curr_texture_pos);
	glColor4f(1.0, 1.0, 1.0, opacity);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, curr_texture);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, SCREEN_W, 0, SCREEN_H, -1, 1);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	drawSprite(x1_coord, y1_coord, x2_coord, y2_coord, 0, 1, 1, 0);

	glMatrixMode(GL_PROJECTION);  // Select The Projection Matrix
	glPopMatrix();                // Restore The Old Projection Matrix

	glMatrixMode(GL_MODELVIEW);  // Select The Modelview Matrix
	glPopMatrix();               // Restore The Old Projection Matrix

	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	glColor4f(1.0, 1.0, 1.0, 1.0);
}

void texture_overlay_at(pTextureList t, char *texture_name, float x1_coord, float y1_coord, float opacity) {
	int curr_texture_pos = getTexturePos(t, texture_name);
	int curr_texture = retrieveTextureHandle(t, curr_texture_pos);

	float x1, y1, x2, y2 = 0;
	x1 = x1_coord;
	y1 = y1_coord;

	x2 = x1 + retrieveTextureWidth(t, curr_texture_pos);
	y2 = y1 + retrieveTextureHeight(t, curr_texture_pos);

	texture_overlay(curr_texture, x1, y1, x2, y2, opacity);
}

void pvr_to_grayscale(pvr_ptr_t texture_ptr, long unsigned int w, long unsigned int h) {
	uint16 *txt_ptr = texture_ptr;

	uint16 r = 0;
	uint16 g = 0;
	uint16 b = 0;

	uint16 grey = 0;

	unsigned int i = 0;

	for(; i < w * h; i++) {
		r = (txt_ptr[i] & 0xF800) >> 11;
		g = (txt_ptr[i] & 0x07E0) >> 5;
		b = (txt_ptr[i] & 0x001F);

		grey = (r + g + b) / 3;

		txt_ptr[i] = grey | (grey << 5) | (grey << 11);
	}
}

void gridDraw(pmesh mesh, pmesh grid, pDemoAssetList asl) {

	int i = 0;

	vec3 *iter_ptr = (vec3 *)grid->vertex_data;  // to make the syntax more tolerable

	float z_value = 0.0f;

	int x_iter = 0;
	int y_iter = 0;

	for(x_iter = 0; x_iter < grid->indices_count; x_iter++) {
		for(y_iter = 0; y_iter < grid->uv_count; y_iter++) {
			z_value = 7 * fsin(asl->milisec_elapsed * 0.0009 + (iter_ptr[i].x / 5));
			// z_value = fsin(asl->milisec_elapsed * 0.0005 + iter_ptr[i].x) + fcos(asl->milisec_elapsed * 0.0005 + iter_ptr[i].y) ;
			// z_value = fsin(iter_ptr[i].x * iter_ptr[i].y * i) ;
			glPushMatrix();
			//  glScalef(0.5, 0.5, 0.5);
			glTranslatef(iter_ptr[i].x, iter_ptr[i].y, z_value);
			// printf("t : x : %f, y: %f  i : %d \n ", iter_ptr[i].x, iter_ptr[i].y, i);
			draw_p(mesh);
			glPopMatrix();
			i++;
		}
	}
}

inline float new_width(const float fwsize, const float fhsize, const float req_size_w) {
	if(fwsize == 0) {
		return 0;
	}
	if(fhsize == 0) {
		return 0;
	}

	return (float)(req_size_w * fwsize) / fhsize;
}

inline float new_height(const float f_old_wsize, const float f_new_wsize, const float f_height) {
	if(f_old_wsize == 0) {
		return 0;
	}
	if(f_new_wsize == 0) {
		return 0;
	}

	return (float)(f_height * f_new_wsize) / f_old_wsize;
}

pTexFontList getTexFont(pTexFontList p, char *a) {
	pTexFontList curr = p;
	for(curr = p; curr != NULL; curr = (pTexFontList)curr->next) {
		if(strcmp(curr->name, a) == 0) {
			// printf("\n seeking : %s ; %s found \n",curr->name, a);
			return curr;
		}
	}

	return p;  // if nothing was found......
}

void print_text(pDemoAssetList d, char *font_name, char *txt, int xpos, int ypos, const float scale) {
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glOrtho(0, SCREEN_W, 0, SCREEN_H, -1, 1);


	glDisable(GL_LIGHTING);
	int curr_texture_pos = getTexturePos(d->t, font_name);

	int curr_texture = retrieveTextureHandle(d->t, curr_texture_pos);

	int tex_w = retrieveTextureWidth(d->t, curr_texture_pos);
	int tex_h = retrieveTextureHeight(d->t, curr_texture_pos);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, curr_texture);

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

	float cursorX = xpos;
	float cursorY = ypos;

	float const spacing = 5;

	pTexFontList f = getTexFont(d->f, font_name);

	char c;
	while((c = *txt++)) {
		TexCharCoord const *const glyph = &f->charCoord[(unsigned)c];
		float const w = glyph->width;
		float const h = glyph->height;
		float const wScaled = w * scale;
		float const hScaled = h * scale;

		float y = cursorY;

		// Lower certain characters
		if(c == 'y' || c == 'g' || c == 'j' || c == 'p' || c == 'q') {
			y -= 8 * scale;
		}

		drawSprite(cursorX, y, cursorX + wScaled, y + hScaled, glyph->x / (float)tex_w, (glyph->y + h) / (float)tex_h, (glyph->x + w) / (float)tex_w, glyph->y / (float)tex_h);

		cursorX += wScaled + spacing;
	}

	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
}

// timming functions -

inline unsigned long diff_msec(uint64 start, uint64 end) {
	if(end > start) {
		return (unsigned long)(end - start);
	} else {
		return (unsigned long)(start - end);
	}
}

void draw_p(pmesh p) {

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, p->vertex_data);

	if(p->uv_data != NULL) {
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2, GL_FLOAT, 0, p->uv_data);
		glEnable(GL_TEXTURE_2D);
	}


	if(p->vertex_normal != NULL)
	{	
		glEnableClientState(GL_NORMAL_ARRAY);
		glNormalPointer(GL_FLOAT, 0, p->vertex_normal);
		//printf("Mesh has normal\n");
	}

	glDrawArrays(GL_TRIANGLES, 0, p->vertex_count);
	glDisableClientState(GL_VERTEX_ARRAY);

	if(p->uv_data != NULL) 
	{
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisable(GL_TEXTURE_2D);
	}

	if(p->vertex_normal != NULL)
	{	
		glDisableClientState(GL_NORMAL_ARRAY);
	}




}


void draw_cube_header()
{
	
	

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, sizeof (struct vertex_struct), &vertices[0].x );

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_FLOAT, sizeof (struct vertex_struct), &(vertices[0].nx) );	


	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, GL_FLOAT,sizeof (struct vertex_struct),&vertices[0].u);

	glDrawElements(GL_TRIANGLES, vertex_count[0], INX_TYPE, indexes);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);


}
void new_draw(pDemoAssetList a) {
	// measures the time it took to render in order to keep the animation time dependent instead of frame rate dependent.
	// the scene descriptor WILL get modified during the demo execution

	uint64 ms_start;
	uint64 ms_end;
	uint64 ms_elapsed;

	pDemoAssetList iter;
	p3DModelList m;
	pTextureList t;
	pSceneDescription d;
	pCameraList c;
	pTransformList tr;
	pStats s ;

	pmesh curr_model;
	pmesh aux_model;
	int curr_texture;
	int curr_texture_pos;

	iter = a;

	

	m = iter->m;  // model list
	t = iter->t;  // texture list
	d = iter->d;  // scene descriptor
	c = iter->c;  // camera descriptor
	s = iter->s;  // stats descriptor

	pSceneDescription d_light = d ;

	ms_start = timer_ms_gettime64();

	
	while(isCameraInTime(c, iter->milisec_elapsed) == 0) {
		c = c->next;  // seeks the camera in the current time frame
	}

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(projectionMatrix);
	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	
	

	
	while(d_light != NULL) //d_light != NULL)
	{
		
		
		if(d_light->render_effects & SIMPLE_LIGHT)
		{
			
	
			for(tr = (pTransformList)d_light->transforms; tr != NULL; tr = (pTransformList)tr->next) {

				if((tr->transformType == LIGHT_DIFFUSE) && (d_light->render_effects & SIMPLE_LIGHT)) {
					GLfloat diffuse_val[] = {tr->transformData.x, tr->transformData.y, tr->transformData.z, 1.0} ;
					
					//printf("light color : %f %f %f \n", tr->transformData.x, tr->transformData.y, tr->transformData.z);
					glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_val);	
				}

				if((tr->transformType == TRANSLATION) && (d_light->render_effects & SIMPLE_LIGHT)) {
					//glTranslatef( -(c->start_pos.x), -(c->start_pos.y), -(c->start_pos.z));
					GLfloat position_val[] = {tr->transformData.x, tr->transformData.y, tr->transformData.z, 0.0} ;
					//GLfloat position_val[] = {1.0, 1.0, 1.0, 0.0} ;
					//printf("light position : %f %f %f \n", tr->transformData.x, tr->transformData.y, tr->transformData.z);
					
					glLightfv(GL_LIGHT0, GL_POSITION, position_val);
				}

			}
			
		}
		
		
		d_light = d_light->next;  // goes to the next element
		
	}

	
	gluLookAt(c->start_pos.x, c->start_pos.y, c->start_pos.z, 
			 c->start_looking_direction.x, c->start_looking_direction.y, 
			 c->start_looking_direction.z, 0.0, 0.0, 1.0);


	//printf("Camera x %f y %f z %f \n",c->start_pos.x, c->start_pos.y, c->start_pos.z );

	
	
	while(d != NULL)  // goes trough all the meshes on the asset list
	{

		glPushMatrix();
		
		if(d->render_effects & DRAW_WIREFRAME) {
			// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		}

		if(d->render_effects & DRAW_FLAT) {
			// glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}

		
		for(tr = (pTransformList)d->transforms; tr != NULL; tr = (pTransformList)tr->next) {

			
			if((tr->transformType == ROTATION) && !(d->render_effects & SIMPLE_LIGHT)) {
				
				
				glRotatef(tr->scalar_val, tr->transformData.x, tr->transformData.y, tr->transformData.z);
				tr->scalar_val++ ;
				
			}
			if((tr->transformType == TRANSLATION) && !(d->render_effects & SIMPLE_LIGHT)) {
				
				glTranslatef(tr->transformData.x , tr->transformData.y , tr->transformData.z );
			}
			if((tr->transformType == SCALE) && !(d->render_effects & SIMPLE_LIGHT)) {
				
				glScalef(tr->transformData.x, tr->transformData.y, tr->transformData.z);
			}


		}
		
		if(d->render_effects & DRAW_REGULAR)  // regular model drawing
		{

			// if fade_in == fade_out , start == end, assume the mesh is "allways alive"
			if((d->start == d->end))
			{
				curr_texture_pos = getTexturePos(t, d->texture_name);
				curr_texture = retrieveTextureHandle(t, curr_texture_pos);
				glBindTexture(GL_TEXTURE_2D, curr_texture);
				curr_model = retrieveMesh(m, getMeshPos(m, d->mesh_name));
				addMeshStat(s, curr_model) ;
				draw_p(curr_model);
				//draw_cube_header(); // pq ?
			}
			else
			{
				if(is_it_on_time(iter->milisec_elapsed, d->start, d->end))
				{
					curr_texture_pos = getTexturePos(t, d->texture_name);
					curr_texture = retrieveTextureHandle(t, curr_texture_pos);
					glBindTexture(GL_TEXTURE_2D, curr_texture);
					curr_model = retrieveMesh(m, getMeshPos(m, d->mesh_name));
					addMeshStat(s, curr_model);
					draw_p(curr_model);
				}
			}
		}
		if(d->render_effects & DRAW_GRID)  // renders a given mesh multiple times on a grid mesh
		{

			curr_texture_pos = getTexturePos(t, d->texture_name);
			curr_texture = retrieveTextureHandle(t, curr_texture_pos);

			glBindTexture(GL_TEXTURE_2D, curr_texture);

			curr_model = retrieveMesh(m, (getMeshPos(m, d->name)));    // the grid mesh. Cant figure out WHY I must -1 on the position.
			aux_model = retrieveMesh(m, getMeshPos(m, d->mesh_name));  // the mesh to be rendered on the grid

			gridDraw(aux_model, curr_model, a);
		}

		if(d->render_effects & DRAW_2D_OVERLAY)  // 2d overlay
		{
			if(is_it_on_time(iter->milisec_elapsed, d->start, d->end)) {
				tr = (pTransformList)d->transforms;
				float opac = opacity_level(iter->milisec_elapsed, d->start, d->end, d->fade_in, d->fade_out);
				//  printf("opacity : %f\n", opac );
				texture_overlay_at(t, d->texture_name, (unsigned int)tr->transformData.x, (unsigned int)tr->transformData.y, opac);
			}
		}

		glPopMatrix();

		d = d->next;  // goes to the next element
		
	}

	
	




	char hackjob[512];
	snprintf(hackjob, sizeof(hackjob), "ms elapsed : %lu ms", iter->milisec_elapsed);
	hackjob[511] = 0;

	print_text(a, "font_2", "Vollumetric Illusions Demo Framework v0.0.1", 10, 40, 0.4);
	print_text(a, "font", hackjob, 10, 80, 0.8);

	snprintf(hackjob, sizeof(hackjob), "vertex_count : %lu ", s->totalVerticesPerFrame);
	
	print_text(a, "font", hackjob, 10, 120, 0.5);



	glutSwapBuffers();

	resetStatsPerFrame(s);

	ms_end = timer_ms_gettime64();

	ms_elapsed = diff_msec(ms_start, ms_end);

	
	// HOTFIX FOR ERROR ON MS_ELAPSED ERROR CALCULATION ITS ONLY OBSERVED ON LXDREAM
	if(ms_elapsed > 100)
	{
		printf("ms_elapsed > 100 \n") ;
		printf("ms_elapsed = %llu ; ms_start = %llu ; ms_end = %llu \n", ms_elapsed, ms_start, ms_end);

	}
	else
	{
		iter->milisec_elapsed = iter->milisec_elapsed + ms_elapsed;
	}
	
	CameraListPosUpdate(iter->c, (iter->milisec_elapsed));

// check for OpenGL errors in debug releases with libgl15 (kgl has no support for glGetError)
#if !defined(NDEBUG) && defined(GLAPIENTRY)
	GLenum error;
	while((error = glGetError())) {
		printf("GL error: %s\n", gluErrorString(error));
	}
#endif
}


int static_light_pass(pDemoAssetList a)
{
	pDemoAssetList iter;
	pSceneDescription d_light;
	pTransformList tr;
	
	iter = a;

	d_light = iter->d;  // scene descriptor
	

	while(d_light != NULL) //d_light != NULL)
	{	
		
		if(d_light->render_effects & SIMPLE_LIGHT)
		{
			//glLoadIdentity();
			glEnable(GL_LIGHTING);
			glEnable(GL_LIGHT0);
			
			for(tr = (pTransformList)d_light->transforms; tr != NULL; tr = (pTransformList)tr->next) {

				if((tr->transformType == LIGHT_DIFFUSE) && (d_light->render_effects & SIMPLE_LIGHT)) {
					GLfloat diffuse_val[] = {tr->transformData.x, tr->transformData.y, tr->transformData.z, 1.0} ;
					glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_val);	
				}

				if((tr->transformType == TRANSLATION) && (d_light->render_effects & SIMPLE_LIGHT)) {
					GLfloat position_val[] = {tr->transformData.x, tr->transformData.y, tr->transformData.z, 0.0} ;
					glLightfv(GL_LIGHT0, GL_POSITION, position_val);
				}

			}
			
		}
		
		d_light = d_light->next;  // goes to the next element
	}
	return 0;
}


int demo_init() {
	glFrontFace(GL_CW);
	//glCullFace(GL_BACK);
	glClearColor(0.5f, 0.5f, 0.5f, 1);
	glColor3f(1.0f, 1.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	//glEnable(GL_KOS_NEARZ_CLIPPING);

	float const aspect = (float)SCREEN_W / SCREEN_H;
	float const fovDegrees = 45.f;
	float const zNear = 0.5f;
	glMatrixMode(GL_PROJECTION);
	setInfinitePerspectiveMat(projectionMatrix, aspect, fovDegrees, zNear);
	glLoadMatrixf(projectionMatrix);
	glMatrixMode(GL_MODELVIEW);

	glShadeModel(GL_FLAT);

	
	return 0;
}

int demo_render(pDemoAssetList a) {
	if(demo_init()) {
		puts("Cannot init demo.");
		return 1;
	}

	//static_light_pass(a);

	while(1) {
		new_draw(a);
	}
}
