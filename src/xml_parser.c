/*****************************************************************
 * outline.c
 *
 * Copyright 1999, Clark Cooper
 * All rights reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the license contained in the
 * COPYING file that comes with the expat distribution.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Read an XML document from standard input and print an element
 * outline on standard output.
 * Must be used with Expat compiled for UTF-8 output.
 */

#include <kos.h>
#include <stdio.h>
#include <expat/expat.h>
#include "data_estructures.h"
#include "procedural.h"

#if defined(__amigaos__) && defined(__USE_INLINE__)
#include <proto/expat.h>
#endif

#ifdef XML_LARGE_SIZE
#if defined(XML_USE_MSC_EXTENSIONS) && _MSC_VER < 1400
#define XML_FMT_INT_MOD "I64"
#else
#define XML_FMT_INT_MOD "ll"
#endif
#else
#define XML_FMT_INT_MOD "l"
#endif

#define BUFFSIZE 8192

char Buff[BUFFSIZE];

int Depth;

int reading = 0;

#define MESH 0x01
#define CAMERA 0x02
#define GRID_S 0x03
#define OVERLAY_SIMPLE 0x05  // since I'm not using the values for bitwise operations.....
#define S_LIGHT 0x06

#define START_ROT 0x04
#define END_ROT 0x08
#define START_FOV 0x10

#define END_FOV 0x20
#define START_LOOK 0x40
#define END_LOOK 0x80

#define STATIC_POS 0x100
#define STATIC_ROT 0x200
#define STATIC_FOV 0x400

#define STATIC_ANGLE 0x800
#define PERTETUAL_ROTATION 0x1000

#define VEL_SET 0x2000

#define UNDEFINED -1

//

// the head pointers must be returned
pSceneDescription sd = NULL;
pSceneDescription sd_head = NULL;
pCameraList cl = NULL;
pCameraList cl_head = NULL;
p3DModelList ml_head = NULL;

vec3 tmp;

// depth 0 - > scene
// depth 1 - > item (mesh / camera / grid)
// depth 2 - > vector information

int setMode(const char *d, int dp) {

	if(strcmp("model", d) == 0) {
		return MESH;
	}
	if(strcmp("camera", d) == 0) {
		return CAMERA;
	}
	if(strcmp("grid_sin", d) == 0) {
		return GRID_S;
	}
	if(strcmp("overlay", d) == 0) {
		return OVERLAY_SIMPLE;
	}
	if(strcmp("s_light", d) == 0) {
		return S_LIGHT;
	}


	return UNDEFINED;
}

void printVec3(vec3 a) { printf("%f %f %f \n", (double)a.x, (double)a.y, (double)a.z); }

vec3 fetchVec3(const char **a) {
	vec3 ret;

	ret.x = (float)atof(a[1]);
	ret.y = (float)atof(a[3]);
	ret.z = (float)atof(a[5]);

	//  printf("fetchVec3 : %f \t %f \t %f\n", (double)ret.x, (double)ret.y, (double)ret.z);

	return ret;
}

static void XMLCALL start(void *data, const char *el, const char **attr) {
	int i;

	// for (i = 0; i < Depth; i++)
	//  printf("  ");

	// printf(" (el) %s \n", el);

	if(Depth == 1)  //
	{
		reading = setMode(el, Depth);
	}

	// TODO : Introduzir as funções para a adição dos elementos as listas ligadas
	/*
  switch (reading)
  {
	case MESH : printf("mesh \t %d\n", Depth);


	break;
	case CAMERA : printf("camera \t %d\n", Depth);

	break;

	default : printf("not found \t %d\n", Depth);
  }
*/
	for(i = 0; attr[i]; i += 2) {
		// printf(" %s='%s'", attr[i], attr[i + 1]);
	}

	

	if((reading == S_LIGHT) && (Depth == 1)) {
		// printf(" name = %s\n mesh = %s \n texture = %s \n",attr[1], attr[3], attr[5] );
		// printf("inicio do modelo\n");

		sd = NULL;

		sd = newSceneDescription(); /* cria um scene_descriptor*/
		addToSceneDescriptionName(sd, attr[1]);
		addToSceneDescriptionMesh(sd, "NONE");
		//addToSceneDescriptionTextureName(sd, attr[3]);
		

		setSceneDescriptionRender(sd, SIMPLE_LIGHT);
		
	}


	if((reading == OVERLAY_SIMPLE) && (Depth == 1)) {
		// printf(" name = %s\n mesh = %s \n texture = %s \n",attr[1], attr[3], attr[5] );
		// printf("inicio do modelo\n");

		sd = NULL;

		sd = newSceneDescription(); /* cria um scene_descriptor*/
		addToSceneDescriptionName(sd, attr[1]);
		addToSceneDescriptionMesh(sd, "NONE");
		addToSceneDescriptionTextureName(sd, attr[3]);
		//addToSceneDescriptor2DoverlayStart(sd, (uint64)atoi(attr[5]));
		//addToSceneDescriptor2DoverlayEnd(sd, (uint64)atoi(attr[7]));
		//addToSceneDescriptor2DoverlayFade_In(sd, (uint64)atoi(attr[9]));
		//addToSceneDescriptor2DoverlayFade_Out(sd, (uint64)atoi(attr[11]));

		setSceneDescriptionRender(sd, DRAW_2D_OVERLAY);
	}

	if((reading == MESH) && (Depth == 1)) {
		//    printf(" name = %s\n mesh = %s \n texture = %s \n",attr[1], attr[3], attr[5] );
		// printf("inicio do modelo\n");
		sd = NULL;

		sd = newSceneDescription(); /* cria um scene_descriptor*/
		addToSceneDescriptionName(sd, attr[1]);
		addToSceneDescriptionMesh(sd, attr[3]);
		addToSceneDescriptionTextureName(sd, attr[5]);
		setSceneDescriptionRender(sd, DRAW_REGULAR);
	}

	if((reading == GRID_S) && (Depth == 1)) {
		//    printf(" name = %s\n mesh = %s \n texture = %s \n x_seg = %s \n y_seg = %s \n x_size = %s \n y_size = %s \n",
		//            attr[1], attr[3], attr[5], attr[7], attr[9], attr[11], attr[13]  );
		// the name will be the name of the generated procedural mesh,
		// the mesh atribube will be the name of the mesh to be rendered at each point of the grid.
		// the texture will unused for now.
		// x_seg number of points on X
		// y_seg number of points on Y
		// x_size - self explenatory
		// y_size - self explenatory

		// get the data on the format needed for the grid generator function

		int x_seg = atoi(attr[7]);
		int y_seg = atoi(attr[9]);

		float x_size = atof(attr[11]);
		float y_size = atof(attr[13]);

		pmesh m = genGrid(attr[1], x_size, y_size, x_seg, y_seg);  // the mesh is created HERE

		ml_head = add_pmesh(ml_head, m, attr[1]);  //  and added here

		sd = NULL;

		sd = newSceneDescription(); /* cria um scene_descriptor*/

		addToSceneDescriptionName(sd, attr[1]);
		addToSceneDescriptionMesh(sd, attr[3]);
		addToSceneDescriptionTextureName(sd, attr[5]);
		setSceneDescriptionRender(sd, DRAW_GRID);  // this is what is used to distinguish this type of mesh - its not meant to be rendered
	}

	if((reading == CAMERA) && (Depth == 1)) {
		cl = NULL;
		cl = newCamera();
		reading = CAMERA;
		setCameraStartTime(cl, atoi(attr[3]));
		setCameraEndTime(cl, atoi(attr[5]));

		// printf("CAM - string : %s\t %s\n" , attr[3] , attr[5] );

		// printf("CAM - conv: %d\t %d\n" , atol(attr[3]) , atol(attr[5]) );
	}

	if((reading == S_LIGHT) && (Depth == 2) && (strcmp("diffuse", el) == 0)) {
		sd->transforms = addTransform(sd->transforms, LIGHT_DIFFUSE, fetchVec3(attr), 0);
	}

	if((reading == S_LIGHT) && (Depth == 2) && (strcmp("specular", el) == 0)) {
		sd->transforms = addTransform(sd->transforms, LIGHT_SPECULAR, fetchVec3(attr), 0);
	}
	
	if((reading == OVERLAY_SIMPLE) && (Depth == 2) && (strcmp("position", el) == 0)) {
		sd->transforms = addTransform(sd->transforms, TRANSLATION, fetchVec3(attr), 0);
	}

	if(((reading == MESH) || (reading == S_LIGHT))  && (Depth == 2) && (strcmp("position", el) == 0)) {
		sd->transforms = addTransform(sd->transforms, TRANSLATION, fetchVec3(attr), 0);
	}

	if((reading == MESH) && (Depth == 2) && (strcmp("rotation", el) == 0)) {
		sd->transforms = addTransform(sd->transforms, ROTATION, fetchVec3(attr), atof(attr[7]));
	}

	if((reading == MESH) && (Depth == 2) && (strcmp("scale", el) == 0)) {
		// MESH - obter vector de posição
		sd->transforms = addTransform(sd->transforms, SCALE, fetchVec3(attr), 0);
	}

	
	
	if(((reading == MESH) || (reading == OVERLAY_SIMPLE) || (reading == S_LIGHT)) && (Depth == 2) && (strcmp("start", el) == 0)) {
		// MESH - start
		//sd->transforms = addTransform(sd->transforms, SCALE, fetchVec3(attr), 0);
		addToSceneDescriptorStartTime(sd, (uint64)atoi(attr[1])); 
		//printf("ATTR :: %s \n \n ", attr[1]);

	}

	if(((reading == MESH) || (reading == OVERLAY_SIMPLE) || (reading == S_LIGHT)) && (Depth == 2) && (strcmp("end", el) == 0)) {
		// MESH - start
		//sd->transforms = addTransform(sd->transforms, SCALE, fetchVec3(attr), 0);
		//printf("ATTR :: %s \n \n ", attr[1]);
		addToSceneDescriptorEndTime(sd, (uint64)atoi(attr[1]));
		
	}

	if(((reading == MESH) || (reading == OVERLAY_SIMPLE) || (reading == S_LIGHT)) && (Depth == 2) && (strcmp("fade_in", el) == 0)) {
		// MESH - start
		//sd->transforms = addTransform(sd->transforms, SCALE, fetchVec3(attr), 0);
		//printf("ATTR :: %s \n \n ", attr[1]);
		addToSceneDescriptorFade_In(sd, (uint64)atoi(attr[1]));
		
	}

	if(((reading == MESH) || (reading == OVERLAY_SIMPLE) || (reading == S_LIGHT)) && (Depth == 2) && (strcmp("fade_out", el) == 0)) {
		// MESH - start
		//sd->transforms = addTransform(sd->transforms, SCALE, fetchVec3(attr), 0);
		//printf("ATTR :: %s \n \n ", attr[1]);
		addToSceneDescriptorFade_Out(sd, (uint64)atoi(attr[1]));
		
	}

	


	if((reading == GRID_S) && (Depth == 2) && (strcmp("position", el) == 0)) {
		sd->transforms = addTransform(sd->transforms, TRANSLATION, fetchVec3(attr), 0);
	}

	if((reading == GRID_S) && (Depth == 2) && (strcmp("rotation", el) == 0)) {
		sd->transforms = addTransform(sd->transforms, ROTATION, fetchVec3(attr), atof(attr[7]));
	}

	if((reading == GRID_S) && (Depth == 2) && (strcmp("scale", el) == 0)) {
		// MESH - obter vector de posição
		sd->transforms = addTransform(sd->transforms, SCALE, fetchVec3(attr), 0);
	}

	if((reading == CAMERA) && (Depth == 2) && (strcmp("start_pos", el) == 0)) {
		// TODO : codigo para recolher os dados da camara
		setCameraPosStart(cl, fetchVec3(attr));
		setCameraFlag(cl, START_POS);
	}

	if((reading == CAMERA) && (Depth == 2) && (strcmp("end_pos", el) == 0)) {
		// TODO : codigo para recolher os dados da camara
		setCameraPosEnd(cl, fetchVec3(attr));
		setCameraFlag(cl, END_POS);
	}

	if((reading == CAMERA) && (Depth == 2) && (strcmp("start_lookat", el) == 0)) {
		// TODO : codigo para recolher os dados da camara
		setCameraLookingDirectionStart(cl, fetchVec3(attr));
		setCameraFlag(cl, START_LOOK);
	}

	if((reading == CAMERA) && (Depth == 2) && (strcmp("end_lookat", el) == 0)) {
		// TODO : codigo para recolher os dados da camara
		setCameraLookingDirectionEnd(cl, fetchVec3(attr));
		setCameraFlag(cl, END_LOOK);
	}

	

	// printf("\n");
	Depth++;
}

static void XMLCALL end(void *data, const char *el) {
	Depth--;
	// printf("END TAG \n");

	if((reading == MESH) && (Depth == 1)) {
		// MESH - adicionar o item a lista
		//      printf("END MESH ITEM\n");
		sd_head = addToSceneDescriptionList(sd_head, sd);
	}

	if((reading == CAMERA) && (Depth == 1)) {
		// MESH - adicionar o item a lista
		//      printf("END CAM ITEM\n");
		cl_head = addToCameraList(cl_head, cl);
	}

	if((reading == GRID_S) && (Depth == 1)) {
		// MESH - adicionar o item a lista
		//      printf("S_GRID\n");
		sd_head = addToSceneDescriptionList(sd_head, sd);
	}
	if((reading == OVERLAY_SIMPLE) && (Depth == 1)) {
		// MESH - adicionar o item a lista
		//      printf("OVERLAY_SIMPLE\n");
		sd_head = addToSceneDescriptionList(sd_head, sd);
	}
	if((reading == S_LIGHT) && (Depth == 1)) {
		// MESH - adicionar o item a lista
		printf("S_LIGHT added\n");
		sd_head = addToSceneDescriptionList(sd_head, sd);
	}

	// printf("\n Depth at end tag : %s \t %d \n", el, Depth);
}

void data_structure_lst(pDemoAssetList a) {
	pSceneDescription d;
	pCameraList c;

	d = a->d;
	c = a->c;

	if(a == NULL) {
		printf("\nNULL POINTER\n");
	}
	while((d != NULL)) {
		//    printf("Scene Descriptor\nName  : %s (%d)\n", d->name, i);
		//  printVec3(d->pos);

		d = d->next;
	}

	while((c != NULL)) {
		//   printf("Camera Descriptor\n Time End : %lu \t Time Start %lu \t (%d) \n", c->time_start, c->time_end, i );
		c = c->next;
	}
}

int init_parser(char *filename, pDemoAssetList assets) {

	// ugly "hack" to allow the introduction of precedually generated meshes into the mesh linked list
	ml_head = assets->m;

	FILE *in_Stream;
	in_Stream = fopen(filename, "r");

	XML_Parser p = XML_ParserCreate(NULL);
	if(!p) {
		fprintf(stderr, "Couldn't allocate memory for parser\n");
		return -1;
	}

	XML_SetElementHandler(p, start, end);

	char buff;
	int done = 0;

	for(fread(&buff, sizeof(char), 1, in_Stream); !feof(in_Stream); fread(&buff, sizeof(char), 1, in_Stream))
		if(!XML_Parse(p, &buff, sizeof(char), done)) {
			fprintf(stderr, "..Parse error at line %lu:\n%s\n", XML_GetCurrentLineNumber(p), XML_ErrorString(XML_GetErrorCode(p)));
			return -1;
		}

	// DEBUG =

	assets->d = sd_head;
	assets->c = cl_head;
	// assets->m = ml_head;

	data_structure_lst(assets);

	return 0;
}
