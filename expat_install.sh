#!/bin/bash

#Enviroment Variables must be set :: environ.sh must be sourced first

START_DIR=$(pwd)

cd $KOS_BASE

cd ../kos-ports

pwd

KOS_PORTS_DIR=$(pwd)

wget http://downloads.sourceforge.net/project/expat/expat/2.1.0/expat-2.1.0.tar.gz -O expat-2.1.0.tar.gz

tar -vxzf expat-2.1.0.tar.gz

cd expat-2.1.0

CC=kos-cc CXX=kos-c++ ./configure --host=sh-elf --prefix=$(KOS_CC_BASE)

make

## At this point I assume expat has compiled fine

cd .libs/

## copy the libexpat.a and libexpat.lai as libexpat.la

cp libexpat.lai $KOS_BASE/addons/lib/dreamcast/libexpat.la

cp libexpat.a $KOS_BASE/addons/lib/dreamcast/libexpat.a

## make a sysbolic link on kos-ports/include the expat.h

cd ..

ln -s $KOS_PORTS_DIR/expat-2.1.0/lib $KOS_PORTS_DIR/include/expat

cd $START_DIR

echo Done


