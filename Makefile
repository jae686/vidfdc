#
# Basic KallistiOS skeleton / test program
# Copyright (C)2001-2004 Dan Potter
#   

SRC_PATH=src/
BIN_PATH=bin/


# Put the filename of the output binary here
TARGET = $(BIN_PATH)main.elf

# List all of your C files here, but change the extension to ".o"
# Include "romdisk.o" if you want a rom disk.
OBJS = $(patsubst $(SRC_PATH)%.c,$(BIN_PATH)%.o,$(wildcard $(SRC_PATH)*.c))

# If you define this, the Makefile.rules will create a romdisk.o for you
# from the named dir.
KOS_ROMDISK_DIR = romdisk

# The rm-elf step is to remove the target before building, to force the
# re-creation of the rom disk.
all: rm-elf $(TARGET)

include $(KOS_BASE)/Makefile.rules

clean:
	-rm -f $(TARGET) $(OBJS) romdisk.*

rm-elf:
	-rm -f $(TARGET) romdisk.*

$(BIN_PATH)%.o: $(SRC_PATH)%.c
	mkdir -p $(BIN_PATH) && kos-cc $(KOS_CFLAGS) -c $< -o $@

$(TARGET): $(OBJS) romdisk.o
	kos-cc -o $(TARGET) $(OBJS) romdisk.o $(OBJEXTRA) -L$(KOS_BASE)/lib  -lGL -lpcx -lkosutils -lexpat -lpng -lz -lm $(KOS_LIBS) 

run: $(TARGET)
	$(KOS_LOADER) $(TARGET)

dist:
	rm -f $(OBJS) romdisk.o romdisk.img
	$(KOS_STRIP) $(TARGET)

